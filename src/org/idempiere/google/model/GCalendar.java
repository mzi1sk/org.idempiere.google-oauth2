package org.idempiere.google.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;

import com.google.api.client.auth.oauth2.Credential;
//import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;



public class GCalendar {
	
    /** Application name. */
	
    private static String APPLICATION_NAME = "";
    
    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY =
        JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

	private static Calendar service = null;
	
	private static X_AD_Useridentities identity=null;

    /** Global instance of the scopes required by this quickstart. */
    private static final List<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);
   
    /** Standard Constructor 
     * @throws IOException 
     * @throws GeneralSecurityException */
    public GCalendar ( X_AD_Useridentities identity) throws GeneralSecurityException, IOException
    {
    	   this.identity=identity;
    	   HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    }

    public static Credential getCredential() throws IOException {
	
        InputStream in =org.idempiere.google.model.GCalendar.class.getResourceAsStream("/org/idempiere/googleoauth2/client_secret.json");
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setClientSecrets(clientSecrets).build();
        credential.setRefreshToken(identity.getIdentityIdentifier());

        
		return credential;
    }

    public static com.google.api.services.calendar.Calendar getCalendarService() throws IOException {
    		Credential credential =getCredential();
    		APPLICATION_NAME=MSysConfig.getValue("GOOGLE_API_APP_NAME",Env.getAD_Client_ID(Env.getCtx()));
    		if(credential!=null)
    			return new com.google.api.services.calendar.Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
    		else
    			throw new AdempiereException("A google account has not been configured.");
    }
     
    public static String addEvent( ArrayList<String>emails,Timestamp startDate,Timestamp endDate,
    		String description,String title, String location) throws IOException{
    	
    	if(!isActivedCalendarEvent())
    		return "Not_Activated";
    	service= null;
    	if (service== null)
    		service =getCalendarService();
    	Event event = new Event();
    	ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
    	event.setSummary(title);
    	event.setLocation(location);
    	for (String email : emails) {
    		if (email !=null || !"".equals(email))
    			attendees.add(new EventAttendee().setEmail(email));
		}
    	event.setAttendees(attendees);
		Date startD = new Date(startDate.getTime());
		Date endD = new Date(endDate == null?startDate.getTime():endDate.getTime());

		DateTime start = new DateTime(startD, TimeZone.getTimeZone("UTC"));
		event.setStart(new EventDateTime().setDateTime(start));
			
		DateTime end = new DateTime(endD, TimeZone.getTimeZone("UTC"));
		event.setEnd(new EventDateTime().setDateTime(end));
			
	    event.setStart(new EventDateTime().setDateTime(start));
	    event.setEnd(new EventDateTime().setDateTime(end));
	    	
	    event.setDescription(description);
	    Event createdEvent = service.events().insert("primary", event).setSendNotifications(true).execute();
	    
	    return createdEvent.getId();
    }
    
    public static void updateEvent(String calendarEvent_ID, ArrayList<String>emails,Timestamp startDate,Timestamp endDate,
    		String description,String title, String location) throws IOException{
    	if(!isActivedCalendarEvent())
    		return;
    	
    	if (service== null)
    		service =getCalendarService();
    	Event event = service.events().get("primary", calendarEvent_ID).execute();

    	if (emails!=null){
	    	ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
	    	event.setSummary(title);
	    	event.setLocation(location);
	    	for (String email : emails) {
	    		attendees.add(new EventAttendee().setEmail(email));
			}
	    	event.setAttendees(attendees);
    	}
    	if(startDate!=null){
    		Date startD = new Date(startDate.getTime());
    		Date endD = new Date(endDate == null?startDate.getTime():endDate.getTime());
			DateTime start = new DateTime(startD, TimeZone.getTimeZone("UTC"));
			event.setStart(new EventDateTime().setDateTime(start));
				
			DateTime end = new DateTime(endD, TimeZone.getTimeZone("UTC"));
			event.setEnd(new EventDateTime().setDateTime(end));
				
		    event.setStart(new EventDateTime().setDateTime(start));
		    event.setEnd(new EventDateTime().setDateTime(end));
    	}
	    if(description !=null) 	
	    	event.setDescription(description);
	    Event updatedEvent = service.events().update("primary", event.getId(), event).setSendNotifications(true).execute();
	    System.out.println(updatedEvent.getUpdated());
    	
    }
  
    public static void deleteEvent(String calendarEvent_ID) throws IOException{
    	if(!isActivedCalendarEvent()){
    		return;
    	}
	    if (service== null)
	   		service =getCalendarService();
	   	service.events().delete("primary", calendarEvent_ID).setSendNotifications(true).execute();
    }
    
	public static boolean isActivedCalendarEvent(){
		boolean sendEvent = true; 
		String value = MSysConfig.getValue("SEND_CALENDAR_EVENT","N/A",Env.getAD_Client_ID(Env.getCtx()),Env.getAD_Org_ID(Env.getCtx()));
		if (value.equals("N"))
			sendEvent = false;
		// Si no existe el configurador del sistema SEND_CALENDAR_EVENT se enviará el evento
		return sendEvent; 
	}
    
}
