/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.idempiere.google.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for AD_Useridentities
 *  @author iDempiere (generated) 
 *  @version Release 4.1 - $Id$ */
public class X_AD_Useridentities extends PO implements I_AD_Useridentities, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171113L;

    /** Standard Constructor */
    public X_AD_Useridentities (Properties ctx, int AD_Useridentities_ID, String trxName)
    {
      super (ctx, AD_Useridentities_ID, trxName);
      /** if (AD_Useridentities_ID == 0)
        {
			setAD_User_ID (0);
			setAD_Useridentities_ID (0);
			setIdentityIdentifier (null);
        } */
    }

    /** Load Constructor */
    public X_AD_Useridentities (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AD_Useridentities[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set User Identities.
		@param AD_Useridentities_ID User Identities	  */
	public void setAD_Useridentities_ID (int AD_Useridentities_ID)
	{
		if (AD_Useridentities_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Useridentities_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Useridentities_ID, Integer.valueOf(AD_Useridentities_ID));
	}

	/** Get User Identities.
		@return User Identities	  */
	public int getAD_Useridentities_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Useridentities_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set AD_Useridentities_UU.
		@param AD_Useridentities_UU AD_Useridentities_UU	  */
	public void setAD_Useridentities_UU (String AD_Useridentities_UU)
	{
		set_ValueNoCheck (COLUMNNAME_AD_Useridentities_UU, AD_Useridentities_UU);
	}

	/** Get AD_Useridentities_UU.
		@return AD_Useridentities_UU	  */
	public String getAD_Useridentities_UU () 
	{
		return (String)get_Value(COLUMNNAME_AD_Useridentities_UU);
	}

	/** AuthTokenProvider AD_Reference_ID=200118 */
	public static final int AUTHTOKENPROVIDER_AD_Reference_ID=200118;
	/** Cloudempiere = I */
	public static final String AUTHTOKENPROVIDER_Cloudempiere = "I";
	/** Amazon AWS/Cognito = AC */
	public static final String AUTHTOKENPROVIDER_AmazonAWSCognito = "AC";
	/** Google = G */
	public static final String AUTHTOKENPROVIDER_Google = "G";
	/** Twitter = T */
	public static final String AUTHTOKENPROVIDER_Twitter = "T";
	/** Linkedin = L */
	public static final String AUTHTOKENPROVIDER_Linkedin = "L";
	/** Set Token Provider.
		@param AuthTokenProvider Token Provider	  */
	public void setAuthTokenProvider (String AuthTokenProvider)
	{

		set_ValueNoCheck (COLUMNNAME_AuthTokenProvider, AuthTokenProvider);
	}

	/** Get Token Provider.
		@return Token Provider	  */
	public String getAuthTokenProvider () 
	{
		return (String)get_Value(COLUMNNAME_AuthTokenProvider);
	}

	/** Set Expire On.
		@param Expiration 
		Expire On
	  */
	public void setExpiration (Timestamp Expiration)
	{
		set_Value (COLUMNNAME_Expiration, Expiration);
	}

	/** Get Expire On.
		@return Expire On
	  */
	public Timestamp getExpiration () 
	{
		return (Timestamp)get_Value(COLUMNNAME_Expiration);
	}

	/** Set Expired.
		@param Expired Expired	  */
	public void setExpired (boolean Expired)
	{
		set_Value (COLUMNNAME_Expired, Boolean.valueOf(Expired));
	}

	/** Get Expired.
		@return Expired	  */
	public boolean isExpired () 
	{
		Object oo = get_Value(COLUMNNAME_Expired);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Identity Email.
		@param IdentityEmail 
		External Identity Provider Email
	  */
	public void setIdentityEmail (String IdentityEmail)
	{
		set_Value (COLUMNNAME_IdentityEmail, IdentityEmail);
	}

	/** Get Identity Email.
		@return External Identity Provider Email
	  */
	public String getIdentityEmail () 
	{
		return (String)get_Value(COLUMNNAME_IdentityEmail);
	}

	/** Set Identity Identifier.
		@param IdentityIdentifier 
		External Identity Provider Identifier
	  */
	public void setIdentityIdentifier (String IdentityIdentifier)
	{
		set_Value (COLUMNNAME_IdentityIdentifier, IdentityIdentifier);
	}

	/** Get Identity Identifier.
		@return External Identity Provider Identifier
	  */
	public String getIdentityIdentifier () 
	{
		return (String)get_Value(COLUMNNAME_IdentityIdentifier);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getIdentityIdentifier()));
    }

	/** Set Verified.
		@param IsVerified 
		The BOM configuration has been verified
	  */
	public void setIsVerified (boolean IsVerified)
	{
		set_Value (COLUMNNAME_IsVerified, Boolean.valueOf(IsVerified));
	}

	/** Get Verified.
		@return The BOM configuration has been verified
	  */
	public boolean isVerified () 
	{
		Object oo = get_Value(COLUMNNAME_IsVerified);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Oauth Credentials.
		@param OauthCredentials Oauth Credentials	  */
	public void setOauthCredentials (String OauthCredentials)
	{
		set_Value (COLUMNNAME_OauthCredentials, OauthCredentials);
	}

	/** Get Oauth Credentials.
		@return Oauth Credentials	  */
	public String getOauthCredentials () 
	{
		return (String)get_Value(COLUMNNAME_OauthCredentials);
	}

	/** Calendar = C */
	public static final String SCOPE_Calendar = "C";
	/** Storage = S */
	public static final String SCOPE_Storage = "S";
	/** Login = L */
	public static final String SCOPE_Login = "L";
	/** Email = E */
	public static final String SCOPE_Email = "E";
	/** Contacts = O */
	public static final String SCOPE_Contacts = "O";
	/** Set Scope.
		@param Scope Scope	  */
	public void setScope (String Scope)
	{

		set_Value (COLUMNNAME_Scope, Scope);
	}

	/** Get Scope.
		@return Scope	  */
	public String getScope () 
	{
		return (String)get_Value(COLUMNNAME_Scope);
	}

	/** Set User Identity Value.
		@param UserIdentityHash User Identity Value	  */
	public void setUserIdentityHash (String UserIdentityHash)
	{
		set_Value (COLUMNNAME_UserIdentityHash, UserIdentityHash);
	}

	/** Get User Identity Value.
		@return User Identity Value	  */
	public String getUserIdentityHash () 
	{
		return (String)get_Value(COLUMNNAME_UserIdentityHash);
	}
}