/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.idempiere.google.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for AD_Useridentities
 *  @author iDempiere (generated) 
 *  @version Release 4.1
 */
@SuppressWarnings("all")
public interface I_AD_Useridentities 
{

    /** TableName=AD_Useridentities */
    public static final String Table_Name = "AD_Useridentities";

    /** AD_Table_ID=4000088 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name AD_Useridentities_ID */
    public static final String COLUMNNAME_AD_Useridentities_ID = "AD_Useridentities_ID";

	/** Set User Identities	  */
	public void setAD_Useridentities_ID (int AD_Useridentities_ID);

	/** Get User Identities	  */
	public int getAD_Useridentities_ID();

    /** Column name AD_Useridentities_UU */
    public static final String COLUMNNAME_AD_Useridentities_UU = "AD_Useridentities_UU";

	/** Set AD_Useridentities_UU	  */
	public void setAD_Useridentities_UU (String AD_Useridentities_UU);

	/** Get AD_Useridentities_UU	  */
	public String getAD_Useridentities_UU();

    /** Column name AuthTokenProvider */
    public static final String COLUMNNAME_AuthTokenProvider = "AuthTokenProvider";

	/** Set Token Provider	  */
	public void setAuthTokenProvider (String AuthTokenProvider);

	/** Get Token Provider	  */
	public String getAuthTokenProvider();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Expiration */
    public static final String COLUMNNAME_Expiration = "Expiration";

	/** Set Expire On.
	  * Expire On
	  */
	public void setExpiration (Timestamp Expiration);

	/** Get Expire On.
	  * Expire On
	  */
	public Timestamp getExpiration();

    /** Column name Expired */
    public static final String COLUMNNAME_Expired = "Expired";

	/** Set Expired	  */
	public void setExpired (boolean Expired);

	/** Get Expired	  */
	public boolean isExpired();

    /** Column name IdentityEmail */
    public static final String COLUMNNAME_IdentityEmail = "IdentityEmail";

	/** Set Identity Email.
	  * External Identity Provider Email
	  */
	public void setIdentityEmail (String IdentityEmail);

	/** Get Identity Email.
	  * External Identity Provider Email
	  */
	public String getIdentityEmail();

    /** Column name IdentityIdentifier */
    public static final String COLUMNNAME_IdentityIdentifier = "IdentityIdentifier";

	/** Set Identity Identifier.
	  * External Identity Provider Identifier
	  */
	public void setIdentityIdentifier (String IdentityIdentifier);

	/** Get Identity Identifier.
	  * External Identity Provider Identifier
	  */
	public String getIdentityIdentifier();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsVerified */
    public static final String COLUMNNAME_IsVerified = "IsVerified";

	/** Set Verified.
	  * The BOM configuration has been verified
	  */
	public void setIsVerified (boolean IsVerified);

	/** Get Verified.
	  * The BOM configuration has been verified
	  */
	public boolean isVerified();

    /** Column name OauthCredentials */
    public static final String COLUMNNAME_OauthCredentials = "OauthCredentials";

	/** Set Oauth Credentials	  */
	public void setOauthCredentials (String OauthCredentials);

	/** Get Oauth Credentials	  */
	public String getOauthCredentials();

    /** Column name Scope */
    public static final String COLUMNNAME_Scope = "Scope";

	/** Set Scope	  */
	public void setScope (String Scope);

	/** Get Scope	  */
	public String getScope();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name UserIdentityHash */
    public static final String COLUMNNAME_UserIdentityHash = "UserIdentityHash";

	/** Set User Identity Value	  */
	public void setUserIdentityHash (String UserIdentityHash);

	/** Get User Identity Value	  */
	public String getUserIdentityHash();
}
