package org.idempiere.google.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.idempiere.google.model.GCalendar;
import org.idempiere.google.model.X_AD_Useridentities;

public class CalendarProcess extends SvrProcess{
	private Timestamp startDate = null;
	private Timestamp endDate = null;
	private String description = null;
	private String title = null;
	private String location = null;
	private ArrayList<String> emails = new ArrayList<String>();
	private String calendarEvent_ID;
	private int AD_Useridentities_ID;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DateFrom"))
				startDate = (Timestamp) para[i].getParameter();
			else if (name.equals("DateTo"))
				endDate = (Timestamp) para[i].getParameter();
			else if (name.equals("Description"))
				description = (String) para[i].getParameter();
			else if (name.equals("Title"))
				title = (String) para[i].getParameter();
			else if (name.equals("EMail"))
				emails.add((String) para[i].getParameter());
			else if (name.equals("LocationComment"))
				location = (String) para[i].getParameter();
			
			else if (name.equals("AD_Useridentities_ID"))
				AD_Useridentities_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		X_AD_Useridentities identity=new X_AD_Useridentities(Env.getCtx(),AD_Useridentities_ID,get_TrxName());
		GCalendar cal=new GCalendar(identity);
		calendarEvent_ID =cal.addEvent(emails, startDate, endDate, description, title, location);
		return "Calendario ID: "+calendarEvent_ID;
	}

}
