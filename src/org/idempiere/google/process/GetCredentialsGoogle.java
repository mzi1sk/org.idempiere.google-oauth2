package org.idempiere.google.process;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import org.compiere.model.MSysConfig;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.idempiere.google.model.GCalendar;
import org.idempiere.google.model.X_AD_Useridentities;
import org.zkoss.zk.ui.Executions;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Preconditions;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.gmail.GmailScopes;

public class GetCredentialsGoogle extends SvrProcess {
	private String description = null;
	private String Scope = null;
	private int p_AD_User_ID;
	private String calendarEvent_ID;
    private static String OAUTH_SCOPE = "https://www.googleapis.com/auth/webmasters.readonly";
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("AD_User_ID"))
				 p_AD_User_ID = para[i].getParameterAsInt();
			else if (name.equals("Scope"))
				Scope= (String) para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		authorize();
		return "Calendario ID: "+calendarEvent_ID;
	}

	private  void  authorize() throws GeneralSecurityException, IOException, URISyntaxException {
    	JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    	
    	List<String> SCOPES =null;
    	if (Scope.equals("C"))
    		SCOPES =Arrays.asList(CalendarScopes.CALENDAR,	"https://www.googleapis.com/auth/userinfo.profile","https://www.googleapis.com/auth/userinfo.email");
    	if (Scope.equals("S"))
    		SCOPES =Arrays.asList(DriveScopes.DRIVE,	"https://www.googleapis.com/auth/userinfo.email");
    	if (Scope.equals("E"))
    		SCOPES =Arrays.asList(GmailScopes.GMAIL_SEND,	"https://www.googleapis.com/auth/userinfo.email");
    	if (Scope.equals("O"))
    		SCOPES =Arrays.asList("https://www.googleapis.com/auth/contacts.readonly",	"https://www.googleapis.com/auth/userinfo.email");
    	if (Scope.equals("L"))
    		SCOPES = Arrays.asList( "https://www.googleapis.com/auth/userinfo.profile",	"https://www.googleapis.com/auth/userinfo.email");
    	
        
    	HttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    	InputStream in = this.getClass().getResourceAsStream("/org/idempiere/googleoauth2/client_secret.json");
    	
    	
    	MemoryDataStoreFactory storeFactory = new MemoryDataStoreFactory();
    	
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
        GoogleAuthorizationCodeFlow flow =new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES).
        		setDataStoreFactory(storeFactory).setAccessType("offline") .build();

        
    	String redirectUri=MSysConfig.getValue("FQDN",Env.getAD_Client_ID(Env.getCtx()))+"/callback/listener";
        X_AD_Useridentities identity=new X_AD_Useridentities(Env.getCtx(),0,null);
        identity.setAD_User_ID(p_AD_User_ID);
        identity.setScope(Scope);
        identity.setAuthTokenProvider(X_AD_Useridentities.AUTHTOKENPROVIDER_Google);
        identity.setUserIdentityHash( UUID.randomUUID().toString().replace("-",""));
        identity.saveEx();	

        AuthorizationCodeRequestUrl authorizationUrl = flow.newAuthorizationUrl().setState(identity.getUserIdentityHash()).setRedirectUri(redirectUri);
        String url = authorizationUrl.build();

        java.net.URI uri = new java.net.URI(url);
		java.awt.Desktop.getDesktop().browse(uri);
		
    
    }
    
    
    
 
}
