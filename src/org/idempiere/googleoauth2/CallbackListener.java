package org.idempiere.googleoauth2;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;

import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;



@WebListener
public class CallbackListener  extends HttpServlet {  

	private GoogleAuthorizationCodeFlow flow;
	private String redirectUri;
	
        public void doGet(HttpServletRequest request, HttpServletResponse response)  
                throws ServletException, IOException {  
      
        	HttpTransport HTTP_TRANSPORT;
			try {
			
        	String code = request.getParameter("code");
        	String state = request.getParameter("state");
        	if (code==null && state==null) {
        		AndCloseServlet(response);
                return;
        	}
        		
   
    	    String url =MSysConfig.getValue("FQDN",Env.getAD_Client_ID(Env.getCtx()))+"/callback/listener"; //get external url FQDN 
        	System.out.println("callback for code :"+ code+" ; state : "+ state+ " for server: "+url); 
        		
          	JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
            List<String> SCOPES =Arrays.asList(CalendarScopes.CALENDAR);

    	    DataStoreFactory storeFactory = new MemoryDataStoreFactory();
    	    HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			
        	InputStream in = org.idempiere.google.model.GCalendar.class.getResourceAsStream("/org/idempiere/googleoauth2/client_secret.json");
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
            
        	GoogleAuthorizationCodeFlow flow =new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES).
             		setDataStoreFactory(  storeFactory).setAccessType("offline") .build();

        	TokenResponse t_response = flow.newTokenRequest(code).setRedirectUri(url).execute();
			flow.createAndStoreCredential(t_response, "user");
			
			GoogleCredential credential = new GoogleCredential().setAccessToken(flow.getCredentialDataStore().get("user").getAccessToken());   
			 Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).setApplicationName(
			          "Oauth2").build();
			 Userinfoplus userinfo = oauth2.userinfo().get().execute();
		
			 
			int identity_updated = DB.executeUpdate("UPDATE AD_Useridentities SET OauthCredentials=?, IdentityIdentifier=?, IdentityEmail=? WHERE UserIdentityHash=? ",
						new Object[]{
									flow.getCredentialDataStore().get("user").getDefaultDataStore(storeFactory).toString(),
									flow.getCredentialDataStore().get("user").getRefreshToken(),
									userinfo.getEmail(),
									state}
			,false,null);
			
			System.out.println("data store foactory :"+ storeFactory.getDataStore("StoredCredential").get("user")); 
			AndCloseServlet(response);
            
			} catch (GeneralSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
            
        }  
        
        private void AndCloseServlet(ServletResponse response) throws IOException{
        	response.setContentType("text/html");  
            
            PrintWriter doc = response.getWriter();
            doc.println("<html>");
            doc.println("<head><title>OAuth 2.0 Authentication Token Recieved</title></head>");
            doc.println("<body>");
            doc.println("Received verification code. Closing...");
            doc.println("<script type='text/javascript'>");
            // We open "" in the same window to trigger JS ownership of it, which lets
            // us then close it via JS, at least in Chrome.
            doc.println("window.setTimeout(function() {");
            doc.println("    window.open('', '_self', ''); window.close(); }, 1000);");
            doc.println("if (window.opener) { window.opener.checkToken(); }");
            doc.println("</script>");
            doc.println("</body>");
            doc.println("</HTML>");
            doc.flush();
        }
      
}
