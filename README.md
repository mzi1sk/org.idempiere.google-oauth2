**Google OAUTH2 plugin **

install plugin, apply 2pack

# System Configurator 

On System level create

1. Name : FQDN (Fully qualified domain name) that has to be also set as Authorized redirect URI in you developer.google.account.
2. Name : GOOGLE_API_APP_NAME  - name of your project https://console.developers.google.com/apis/credentials?project

*Plugin will run as a servlet on your jetty server. Listening on yuor_server:port/callback/listener for credintials from google.*

---

## Process GetCredentialsGoogle

Will start process for getting google credentials. 
Supported  SCOPE at this moment 


1. Google Calendar 
2. Google Drive
3. Reading contacts from a google account
4. User verification ( intended for SSO , to use google account for user login into iDempiere )
5. GMail - only for mailing from the account

---

## Table AD_UserIdentities

Used for storing credentials from google 



